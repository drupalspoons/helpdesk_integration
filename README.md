# Installation

Core patch required: [Comments as base field](https://www.drupal.org/project/drupal/issues/2855068)

Tested patch: https://www.drupal.org/files/issues/2019-04-11/2855068-base-29.patch
