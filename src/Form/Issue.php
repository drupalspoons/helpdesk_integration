<?php

namespace Drupal\helpdesk_integration\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the helpdesk issue entity edit forms.
 */
class Issue extends ContentEntityForm {

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    $this->messenger()->addStatus($this->t('New helpdesk issue %label has been created.', $message_arguments));
    $this->logger('helpdesk_integration')->notice('Created new helpdesk issue %label', $logger_arguments);

    $form_state->setRedirect('entity.helpdesk_issue.canonical', ['helpdesk_issue' => $entity->id()]);
  }

}
