<?php

namespace Drupal\helpdesk_integration\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\helpdesk_integration\Service;
use Drush\Commands\DrushCommands;

/**
 * Drush commandfile.
 */
class SyncCommands extends DrushCommands {

  use StringTranslationTrait;

  /**
   * The helpdesk services.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * SyncCommands constructor.
   *
   * @param \Drupal\helpdesk_integration\Service $service
   *   The helpdesk services.
   */
  public function __construct(Service $service) {
    parent::__construct();
    $this->service = $service;
  }

  /**
   * List all configured helpdesk instances.
   *
   * @usage helpdesk:list
   *   List all configured helpdesk instances.
   *
   * @command helpdesk:list
   */
  public function list() {
    $rows = [];
    /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
    foreach ($this->service->getHelpdeskInstances() as $helpdesk) {
      $rows[] = [
        'id' => $helpdesk->id(),
        'label' => $helpdesk->label(),
        'status' => $helpdesk->status() ? $this->t('enabled') : $this->t('disabled'),
        'default' => $helpdesk->isDefault() ? $this->t('default') : '',
      ];
    }

    return new RowsOfFields($rows);
  }

  /**
   * Synchronize/push user accounts to all configured helpdesk instances.
   *
   * @usage helpdesk:sync:users
   *   Synchronize/push user accounts to all configured helpdesk instances.
   *
   * @command helpdesk:sync:users
   */
  public function syncUsers() {
    $this->service->sync('users');
  }

}
