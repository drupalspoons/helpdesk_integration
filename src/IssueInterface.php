<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a helpdesk issue entity type.
 */
interface IssueInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the helpdesk issue title.
   *
   * @return string
   *   Title of the helpdesk issue.
   */
  public function getTitle(): string;

  /**
   * Sets the helpdesk issue title.
   *
   * @param string $title
   *   The helpdesk issue title.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   The called helpdesk issue entity.
   */
  public function setTitle($title): IssueInterface;

  /**
   * Gets the helpdesk issue creation timestamp.
   *
   * @return int
   *   Creation timestamp of the helpdesk issue.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the helpdesk issue creation timestamp.
   *
   * @param int $timestamp
   *   The helpdesk issue creation timestamp.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   The called helpdesk issue entity.
   */
  public function setCreatedTime($timestamp): IssueInterface;

  /**
   * Add a comment to this issue during remote sync.
   *
   * @param string $extid
   *   The external ID of the comment.
   * @param string $body
   *   The comment body.
   * @param \Drupal\User\UserInterface $user
   *   The user entity of the comment author.
   * @param int $created
   *   The timestamp when this comment was created.
   * @param int $changed
   *   The timestamp when this comment was changed.
   *
   * @return int
   *   The index of this comment in the comments array.
   */
  public function addComment($extid, $body, UserInterface $user, $created, $changed): int;

  /**
   * Add an attachment to a comment during remote sync.
   *
   * @param int $comment_id
   *   The index of this comment in the comments array.
   * @param string $file_name
   *   The filename of the attachment.
   * @param string $url
   *   The url from where to load the attachment's content.
   * @param string $thumbnail_url
   *   Optional, the url from where to load an attachment's thumbnail.
   *
   * @return \Drupal\helpdesk_integration\IssueInterface
   *   This issue entity.
   */
  public function addAttachment($comment_id, $file_name, $url, $thumbnail_url = NULL): IssueInterface;

  /**
   * Get the list of comments during remote sync.
   *
   * @return array
   *   The list of comments.
   */
  public function getComments(): array;

  /**
   * Determine if a user is contained in the access list for this issue.
   *
   * @param int $uid
   *   The id of the user entity to check.
   *
   * @return bool
   *   TRUE if the user is on the list, FALSE otherwise.
   */
  public function hasUser($uid): bool;

  /**
   * Add a user to the access list for this issue if necessary.
   *
   * @param int $uid
   *   The id of the user entity to add.
   *
   * @return bool
   *   TRUE if the user had to be added, FALSE if that user has already been on
   *   the list.
   */
  public function addUser($uid): bool;

  /**
   * Remove a user from the access list for this issue if necessary.
   *
   * @param int $uid
   *   The id of the user entity to remove.
   *
   * @return bool
   *   TRUE if the user had to be removed, FALSE if that user has not been on
   *   the list.
   */
  public function removeUser($uid): bool;

}
