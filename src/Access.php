<?php

namespace Drupal\helpdesk_integration;

use Drupal;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;

/**
 * Access controller.
 */
class Access {

  /**
   * TBD.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function availableHelpdesk(): AccessResult {
    return AccessResult::allowedIf(count(Drupal::service('helpdesk_integration.service')->getHelpdeskInstances()) > 0);
  }

  /**
   * TBD.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   Object to determine if access is allowed.
   */
  public static function applicableHelpdeskForUser(): AccessResult {
    return AccessResult::allowedIf(count(Drupal::service('helpdesk_integration.service')->getHelpdeskInstancesForUser(User::load(Drupal::currentUser()->id()))) > 0);
  }

}
