<?php

namespace Drupal\helpdesk_integration;

use Drupal\comment\Entity\Comment;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginBase as CorePluginBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\Entity\EntityBase;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\media\Entity\Media;
use Drupal\user\Entity\User;
use Drupal\user\UserDataInterface;
use Drupal\user\UserInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Base class for helpdesk_integration plugins.
 */
abstract class PluginBase extends CorePluginBase implements PluginInterface, ContainerFactoryPluginInterface {

  const REMOTE_ID = 'remote_id';

  /**
   * TBD.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * TBD.
   *
   * @var \Drupal\helpdesk_integration\Service
   */
  protected $service;

  /**
   * TBD.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * TBD.
   *
   * @var \Drupal\user\UserDataInterface
   */
  protected $userData;

  /**
   * TBD.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * TBD.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * TBD.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * TBD.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * PluginBase constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   HTTP client factory.
   * @param \Drupal\helpdesk_integration\Service $service
   *   Helpdesk services.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager service.
   * @param \Drupal\user\UserDataInterface $user_data
   *   User data service to persistently store data for each user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   Drupal's time service.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   Drupal's date formatter service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Drupal's logger service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The currently logged in user account session.
   */
  public function __construct(array $configuration, string $plugin_id, $plugin_definition, ClientFactory $client_factory, Service $service, EntityTypeManagerInterface $entity_type_manager, UserDataInterface $user_data, TimeInterface $time, DateFormatterInterface $date_formatter, LoggerInterface $logger, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->clientFactory = $client_factory;
    $this->service = $service;
    $this->entityTypeManager = $entity_type_manager;
    $this->userData = $user_data;
    $this->time = $time;
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client_factory'),
      $container->get('helpdesk_integration.service'),
      $container->get('entity_type.manager'),
      $container->get('user.data'),
      $container->get('datetime.time'),
      $container->get('date.formatter'),
      $container->get('logger.factory')->get('helpdesk'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    return (string) $this->pluginDefinition['label'];
  }

  /**
   * {@inheritdoc}
   */
  public function findUserByData(HelpdeskInterface $helpdesk, string $key, $data): ?UserInterface {
    static $users = [];
    $hash = is_scalar($data) ? $data : hash('md5', $data);
    if (empty($users[$helpdesk->id()][$key][$hash])) {
      $load_uid = 1;
      $data_key = implode(' ', [$helpdesk->id(), $key]);
      foreach ($this->userData->get('helpdesk_integration', NULL, $data_key) as $uid => $value) {
        if ($value === $data) {
          $load_uid = $uid;
          break;
        }
      }
      /** @var \Drupal\user\UserInterface $user */
      $users[$helpdesk->id()][$key][$hash] = User::load($load_uid);
    }
    return $users[$helpdesk->id()][$key][$hash];
  }

  /**
   * {@inheritdoc}
   */
  public function getUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key) {
    $data_key = implode(' ', [$helpdesk->id(), $key]);
    return $this->userData->get('helpdesk_integration', $user->id(), $data_key);
  }

  /**
   * {@inheritdoc}
   */
  public function setUserData(UserInterface $user, HelpdeskInterface $helpdesk, string $key, $value): void {
    $data_key = implode(' ', [$helpdesk->id(), $key]);
    $this->userData->set('helpdesk_integration', $user->id(), $data_key, $value);
  }

  /**
   * {@inheritdoc}
   */
  final public function updateAllIssues(HelpdeskInterface $helpdesk, UserInterface $user = NULL, $force = FALSE): void {
    if ($user === NULL) {
      $user = User::load($this->currentUser->id());
    }
    $last_sync = (int) $this->getUserData($user, $helpdesk, 'last_sync');
    $current = $this->time->getRequestTime();
    if (!$force && ($last_sync + self::REFRESH_INTERVAL >= $current)) {
      return;
    }
    $this->setUserData($user, $helpdesk, 'last_sync', $current);

    try {
      $issueStorage = $this->entityTypeManager->getStorage('helpdesk_issue');
      $commentStorage = $this->entityTypeManager->getStorage('comment');
    } catch (InvalidPluginDefinitionException |  PluginNotFoundException $e) {
      throw new HelpdeskPluginException($e);
    }
    $remoteIssueIds = [];
    foreach ($this->getAllIssues($helpdesk, $user, $last_sync) as $issue) {
      $changed = FALSE;
      /** @var \Drupal\helpdesk_integration\IssueInterface $localIssue */
      if ($localIssues = $issueStorage->loadByProperties([
        'helpdesk' => $helpdesk->id(),
        'extid' => $issue->get('extid')->value,
      ])) {
        $localIssue = array_shift($localIssues);
        // Update issue fields if necessary.
        foreach ([
                   'resolved',
                   'title',
                   'status',
                   'changed',
                   'body',
                 ] as $field_name) {
          if ($localIssue->get($field_name)->value !== $issue->get($field_name)->value) {
            $localIssue->set($field_name, $issue->get($field_name)->value);
            $changed = TRUE;
          }
        }
      }
      else {
        $localIssue = $issue;
        $changed = TRUE;
      }
      try {
        if ($localIssue->addUser($user->id()) || $changed) {
          $localIssue->save();
        }
      } catch (EntityStorageException $e) {
        throw new HelpdeskPluginException($e);
      }
      $remoteIssueIds[] = $localIssue->id();

      // Sync comments.
      $remoteCommentIds = [];
      foreach ($issue->getComments() as $comment) {
        $changed = FALSE;
        /** @var \Drupal\Comment\CommentInterface $localComment */
        if ($localComments = $commentStorage->loadByProperties([
          'entity_type' => 'helpdesk_issue',
          'entity_id' => $localIssue->id(),
          'field_extid' => $comment['extid'],
        ])) {
          $localComment = array_shift($localComments);
          // Update comment fields if necessary.
          foreach ([
                     'uid' => 'author',
                     'comment_body' => 'body',
                     'created' => 'created',
                     'changed' => 'changed',
                   ] as $local_field_name => $remote_field_name) {
            if ($localComment->get($local_field_name)->value !== $comment[$remote_field_name]) {
              $localComment->set($local_field_name, $comment[$remote_field_name]);
              $changed = TRUE;
            }
          }
        }
        else {
          $localComment = Comment::create([
            'entity_type' => 'helpdesk_issue',
            'entity_id' => $localIssue->id(),
            'comment_type' => 'helpdesk_issue_comment',
            'field_name' => 'comment',
            'field_extid' => $comment['extid'],
            'uid' => $comment['author']->id(),
            'created' => $comment['created'],
            'changed' => $comment['changed'],
            'comment_body' => [
              'value' => $comment['body'],
              'format' => 'basic_html',
            ],
          ]);
          $changed = TRUE;
        }
        try {
          if ($changed) {
            $localComment->save();
          }
        } catch (EntityStorageException $e) {
          throw new HelpdeskPluginException($e);
        }
        $remoteCommentIds[] = $localComment->id();

        $this->removeExistingLocalComments($commentStorage, $localIssue, $remoteCommentIds);
        foreach ($comment['attachments'] as $attachment) {
          $file_system = \Drupal::service('file_system');
          $file = file_save_data(file_get_contents($attachment['url']),
            'private://' . $attachment['file_name'],
            FileSystemInterface::EXISTS_REPLACE);

          /** @var \Drupal\media\Entity\Media $media */
          $media = Media::create([
            'bundle' => 'image',
            'uid' => \Drupal::currentUser()->id(),
            'langcode' => \Drupal::languageManager()
              ->getDefaultLanguage()
              ->getId(),
            'field_image' => $file,
            'thumbnail' => [
              'target_id' => $file->id(),
              'alt' => $attachment['file_name'],
            ],
            'field_media_file' => [
              'target_id' => $file->id(),
              'alt' => $attachment['file_name'],
            ],
            'field_media_image' => [
              'target_id' => $file->id(),
              'alt' => $attachment['file_name'],
            ],
          ]);
          try {
            $media->setName($attachment['file_name'])->save();
            $localComment->set('field_attachments', $media);
          } catch (EntityStorageException $e) {
            throw new HelpdeskPluginException($e);
          }

          $config = $media->getSource()->getConfiguration();
        }
        // TODO: Handle attachments, see drupalspoons/helpdesk_integration#17.
      }
    }

    $this->removeAccessOfExistingItems($issueStorage, $helpdesk, $user, $remoteIssueIds);
  }

  /**
   * {@inheritdoc}
   */
  final public function updateAllUsers(HelpdeskInterface $helpdesk, UserInterface $user = NULL, array &$batch = NULL): void {
    $users = [];
    if ($user === NULL) {
      /** @var \Drupal\user\UserInterface $user */
      foreach (User::loadMultiple() as $user) {
        if ($helpdesk->hasAccess($user)) {
          $users[] = $user;
        }
      }
    }
    else {
      $users[] = $user;
    }

    if ($batch !== NULL) {
      $batch['operations'][] = [
        'helpdesk_integration_batch_push_users',
        [$helpdesk, $users],
      ];
      return;
    }
    try {
      foreach ($users as $user) {
        $this->pushUser($helpdesk, $user);
      }
    } catch (HelpdeskPluginException $e) {
      $this->logger->alert('Pushing users to @helpdesk failed: @exception', [
        '@helpdesk' => $helpdesk->label(),
        '@exception' => $e->getMessage(),
      ]);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getRemoteUserId(HelpdeskInterface $helpdesk, UserInterface $user): string {
    static $remoteIds = [];
    if (empty($remoteIds[$helpdesk->id()][$user->id()])) {
      $remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID);
      if ($remote_id === NULL) {
        $this->pushUser($helpdesk, $user);
        $remote_id = $this->getUserData($user, $helpdesk, self::REMOTE_ID);
      }
      $remoteIds[$helpdesk->id()][$user->id()] = $remote_id;
    }
    return $remoteIds[$helpdesk->id()][$user->id()];
  }

  /**
   * Remove access to existing items that no longer apply.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $issueStorage
   * @param \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk
   * @param \Drupal\Core\Entity\EntityBase|null $user
   * @param array $remoteIssueIds
   *
   * @return void
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function removeAccessOfExistingItems(EntityStorageInterface $issueStorage, HelpdeskInterface $helpdesk, EntityBase $user, array $remoteIssueIds) {
    $localIssues = $issueStorage->loadByProperties([
      'helpdesk' => $helpdesk->id(),
      'users' => $user->id(),
    ]);
    $removeIssueIds = array_diff(array_keys($localIssues), $remoteIssueIds);
    /** @var \Drupal\helpdesk_integration\IssueInterface $removeIssue */
    foreach ($removeIssueIds as $removeIssueId) {
      $removeIssue = $localIssues[$removeIssueId];
      try {
        if ($removeIssue->removeUser($user->id())) {
          $removeIssue->save();
        }
      } catch (EntityStorageException $e) {
        throw new HelpdeskPluginException($e);
      }
    }
  }

  /**
   * Remove existing local comments that no longer exist remotely.
   *
   * @param \Drupal\Core\Entity\EntityStorageInterface $commentStorage
   * @param \Drupal\Core\Entity\EntityInterface $localIssue
   * @param array $remoteCommentIds
   *
   * @throws \Drupal\helpdesk_integration\HelpdeskPluginException
   */
  private function removeExistingLocalComments(EntityStorageInterface $commentStorage, EntityInterface $localIssue, array $remoteCommentIds) {
    $localComments = $commentStorage->loadByProperties([
      'entity_type' => 'helpdesk_issue',
      'entity_id' => $localIssue->id(),
    ]);
    $removeCommentIds = array_diff(array_keys($localComments), $remoteCommentIds);
    /** @var \Drupal\Comment\CommentInterface $removeComment */
    try {
      foreach ($removeCommentIds as $removeCommentId) {
        $localComments[$removeCommentId]->delete();
      }
    } catch (EntityStorageException $e) {
      throw new HelpdeskPluginException($e);
    }
  }

}
