<?php

namespace Drupal\helpdesk_integration;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\user\UserInterface;

/**
 * Provides an interface defining a helpdesk entity type.
 */
interface HelpdeskInterface extends ConfigEntityInterface {

  /**
   * Reads and prepares extra settings of helpdesk plugin.
   *
   * @return \Drupal\helpdesk_integration\HelpdeskInterface
   *   This helpdesk entity.
   */
  public function postLoadEntity(): HelpdeskInterface;

  /**
   * Determine if the helpdesk entity is the default one.
   *
   * @return bool
   *   TRUE, if this is the default, FALSE otherwise.
   */
  public function isDefault(): bool;

  /**
   * Gets the plugin for this helpdesk entity.
   *
   * @return \Drupal\helpdesk_integration\PluginInterface
   *   The plugin associated with this helpdesk instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getPlugin(): PluginInterface;

  /**
   * Determine if the given user has access to this helpdesk instance.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user entity for which to determine, if they had access.
   *
   * @return bool
   *   TRUE, if the user has access to this helpdesk instance, FALSE otherwise.
   */
  public function hasAccess(UserInterface $user): bool;

}
