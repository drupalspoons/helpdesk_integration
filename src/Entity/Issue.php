<?php

namespace Drupal\helpdesk_integration\Entity;

use Drupal;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\helpdesk_integration\IssueInterface;
use Drupal\user\UserInterface;

/**
 * Defines the helpdesk issue entity class.
 *
 * @ContentEntityType(
 *   id = "helpdesk_issue",
 *   label = @Translation("Helpdesk Issue"),
 *   label_collection = @Translation("Helpdesk Issues"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\helpdesk_integration\Form\Issue",
 *       "edit" = "Drupal\helpdesk_integration\Form\Issue"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "access" = "Drupal\helpdesk_integration\IssueAccessControlHandler",
 *   },
 *   base_table = "helpdesk_issue",
 *   admin_permission = "administer helpdesk",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/helpdesk/issue/add",
 *     "canonical" = "/helpdesk/issue/{helpdesk_issue}",
 *     "edit-form" = "/helpdesk/issue/{helpdesk_issue}/edit"
 *   },
 *   field_ui_base_route = "entity.helpdesk.collection"
 * )
 */
class Issue extends ContentEntityBase implements IssueInterface {

  use EntityChangedTrait;

  /**
   * Storing comments during remote sync.
   *
   * @var array
   */
  private $syncComments = [];

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => Drupal::currentUser()->id(),
      'comment' => 2,
      'resolved' => FALSE,
      'users' => [Drupal::currentUser()->id()],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);
    if ($this->isNew() && empty($this->get('extid')->value)) {
      /** @var \Drupal\helpdesk_integration\HelpdeskInterface $helpdesk */
      $helpdesk = Drupal::service('helpdesk_integration.service')
        ->getHelpdesk($this);
      $this->set('helpdesk', $helpdesk->id());
      $helpdesk->getPlugin()->createIssue($helpdesk, $this);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title): IssueInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp): IssueInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $user) {
    $this->set('uid', $user->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function addComment($extid, $body, $user, $created, $changed): int {
    $id = count($this->syncComments);
    $this->syncComments[] = [
      'extid' => $extid,
      'body' => $body,
      'author' => $user,
      'created' => $created,
      'changed' => $changed,
      'attachments' => [],
    ];
    return $id;
  }

  /**
   * {@inheritdoc}
   */
  public function addAttachment($comment_id, $file_name, $url, $thumbnail_url = NULL): IssueInterface {
    $this->syncComments[$comment_id]['attachments'][] = [
      'file_name' => $file_name,
      'url' => $url,
      'thumbnail_url' => $thumbnail_url,
    ];
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getComments(): array {
    return $this->syncComments;
  }

  /**
   * {@inheritdoc}
   */
  public function hasUser($uid): bool {
    foreach ($this->get('users')->getValue() as $user) {
      if ($user['target_id'] === $uid) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function addUser($uid): bool {
    if (!$this->hasUser($uid)) {
      $users = $this->get('users')->getValue();
      $users[] = ['target_id' => $uid];
      $this->set('users', $users);
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function removeUser($uid): bool {
    $users = $this->get('users')->getValue();
    foreach ($users as $key => $user) {
      if ($user['target_id'] === $uid) {
        unset($users[$key]);
        $this->set('users', $users);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['helpdesk'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Helpdesk ID'))
      ->setDescription(t('The id of the helpdesk instance in which this helpdesk issue originates from.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['extid'] = BaseFieldDefinition::create('string')
      ->setLabel(t('External ID'))
      ->setDescription(t('The external id of the helpdesk issue entity.'))
      ->setSetting('max_length', 255);

    $fields['resolved'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Resolved'))
      ->setDescription(t('Indicates if helpdesk issues is marked as being resolved.'));

    $fields['users'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Users'))
      ->setDescription(t('The user IDs of all users that are permitted to access the helpdesk issue.'))
      ->setSetting('target_type', 'user')
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the helpdesk issue entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Status'))
      ->setDescription(t('The status of the helpdesk issue entity.'))
      ->setSetting('max_length', 255)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'string',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the helpdesk issue was created.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 1,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the helpdesk issue was last edited.'))
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'timestamp',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('view', TRUE);
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the helpdesk issue author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'entity_reference_label',
        'weight' => 3,
        'settings' => [
          'link' => FALSE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['body'] = BaseFieldDefinition::create('text_long')
      ->setLabel(t('Body'))
      ->setDescription(t('The body text of the helpdesk issue.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 2,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'hidden',
        'weight' => 6,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['attachments'] = BaseFieldDefinition::create('file')
      ->setLabel(t('Attachments'))
      ->setDescription(t('The file IDs of the helpdesk issue attachments.'))
      ->setSettings([
        'display_field' => FALSE,
        'display_default' => FALSE,
        'uri_scheme' => 'private',
        'target_type' => 'file',
        'file_directory' => '[date:custom:Y]-[date:custom:m]',
        'file_extensions' => 'txt pdf jpg gif png zip',
        'max_filesize' => '',
        'description_field' => FALSE,
        'handler' => 'default:file',
      ])
      ->setCardinality(BaseFieldDefinition::CARDINALITY_UNLIMITED)
      ->setDisplayOptions('form', [
        'type' => 'file_generic',
        'settings' => [
          'progress_indicator' => 'throbber',
        ],
        'weight' => 3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'file_table',
        'weight' => 7,
        'settings' => [
          'use_description_as_link_text' => TRUE,
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['comment'] = BaseFieldDefinition::create('comment')
      ->setLabel(t('Comments'))
      ->setDescription(t('The comment IDs of the helpdesk issue comments.'))
      ->setSettings([
        'comment_type' => 'helpdesk_issue_comment',
        'default_mode' => 0,
        'per_page' => '50',
        'anonymous' => FALSE,
        'form_location' => TRUE,
        'preview' => FALSE,
      ])
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'comment_default',
        'weight' => 8,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
